package com.subdue.configamajig;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import com.subdue.configamajig.CryptoUtil.SharedData;

public class IntegrationTest {
	private static final String MAJIG_REALM = "junit";
	private static final String MAJIG_PASSWORD = "shared_secret";
	private static final String MAJIG_SALT64 = "base64_salt";
	private static final String MAJIG_PORT = "9999";

	@SuppressWarnings("deprecation")
	@Test
	public void testMain() throws Exception {
		System.setProperty("MAJIG_REALM", MAJIG_REALM);
		System.setProperty("MAJIG_PASSWORD", MAJIG_PASSWORD);
		System.setProperty("MAJIG_SALT64", MAJIG_SALT64);
		System.setProperty("MAJIG_PORT", MAJIG_PORT);

		Thread mainThread = new Thread(new Runnable() {
			public void run() {
				try {
					Main.main(new String[] { });
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});

		mainThread.start();

		try {
			Thread.sleep(1000);

			String key = "/foo/bar";
			String value = UUID.randomUUID().toString();

			String initialValue = executeGet(key);

			executePost(key, value);

			String finalValue = executeGet(key);

			assertNotEquals(initialValue, finalValue);
			assertEquals(value, finalValue);
		} finally {
			mainThread.stop();
		}
	}

	private String executeGet(String key) throws Exception {
		String requestUrl = "http://localhost:" + MAJIG_PORT + key;
		URL url = new URL(requestUrl);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();

		connection.setDoOutput(false);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod("GET");
		connection.setUseCaches(false);

		int statusCode = connection.getResponseCode();

		if(HttpServletResponse.SC_NOT_FOUND == statusCode) {
			return null;
		}

		if(HttpServletResponse.SC_OK != statusCode) {
			throw new Exception("Get status: " + statusCode);
		}

		StringBuffer responseBuffer = new StringBuffer();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		try {
			String responseLine;

			while((responseLine = bufferedReader.readLine()) != null) {
				responseBuffer.append(responseLine);
			}
		} finally {
			bufferedReader.close();
		}

		String response = responseBuffer.toString();
		String[] pairs = response.split("&");
		String iv64Encoded = null;
		String cipher64Encoded = null;

		for(String pair : pairs) {
			int idx = pair.indexOf("=");
			String param = URLDecoder.decode(pair.substring(0, idx), "UTF-8");
			String paramValue = URLDecoder.decode(pair.substring(idx + 1), "UTF-8");

			if("iv64".equals(param)) {
				iv64Encoded = paramValue;
			} else if("cipher64".equals(param)) {
				cipher64Encoded = paramValue;
			}
		}

		SharedData sharedData = new SharedData(Base64.decodeBase64(iv64Encoded), Base64.decodeBase64(cipher64Encoded));
		char[] password = MAJIG_PASSWORD.toCharArray();
		byte[] salt = Base64.decodeBase64(MAJIG_SALT64);

		return CryptoUtil.decrypt(password, salt, sharedData);
	}

	private void executePost(String key, String value) throws Exception {
		char[] password = MAJIG_PASSWORD.toCharArray();
		byte[] salt = Base64.decodeBase64(MAJIG_SALT64);
		SharedData sharedData = CryptoUtil.encrypt(password, salt, value);
		String iv64 = Base64.encodeBase64String(sharedData.getIv());
		String cipher64 = Base64.encodeBase64String(sharedData.getCiphertext());
		String iv64Encoded = URLEncoder.encode(iv64, "UTF-8");
		String cipher64Encoded = URLEncoder.encode(cipher64, "UTF-8");
		String postData = "iv64=" + iv64Encoded + "&cipher64=" + cipher64Encoded;
		String requestUrl = "http://localhost:" + MAJIG_PORT + key;
		URL url = new URL(requestUrl);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();

		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Charset", "utf-8");
		connection.setRequestProperty("Content-Length", "" + postData.getBytes().length);
		connection.setUseCaches(false);

		try {
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());

			try {
				wr.writeBytes(postData);
				wr.flush();
			} finally {
				wr.close();
			}

			int statusCode = connection.getResponseCode();

			if(HttpServletResponse.SC_OK != statusCode) {
				throw new Exception("Post status: " + statusCode);
			}
		} finally {
			connection.disconnect();
		}
	}
}
