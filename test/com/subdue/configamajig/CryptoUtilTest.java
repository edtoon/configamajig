package com.subdue.configamajig;

import static org.junit.Assert.assertEquals;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import com.subdue.configamajig.CryptoUtil.SharedData;

public class CryptoUtilTest {
	@Test
	public void testEncryptAndDecrypt() throws Exception {
		char[] password = "unit test password".toCharArray();
		byte[] salt = Base64.decodeBase64("mysalt64");
		String data = "Hello from our unit test!";

		SharedData sharedData = CryptoUtil.encrypt(password, salt, data);
		String plaintext = CryptoUtil.decrypt(password, salt, sharedData);

		assertEquals(data, plaintext);
	}
}
