package com.subdue.configamajig;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.subdue.configamajig.CryptoUtil.SharedData;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Main extends AbstractHandler {
	private static final Logger log = LoggerFactory.getLogger(Main.class);

	private final char[] password;
	private final byte[] salt;
	private final String prepend;
	private final JedisPool jedisPool;

	public Main(char[] password, byte[] salt, String realm, JedisPool jedisPool) {
		this.password = password;
		this.salt = salt;
		this.prepend = "configamajig:" + realm + ":";
		this.jedisPool = jedisPool;
	}

	private boolean handleGet(String target, HttpServletResponse response) throws Exception {
		Jedis jedis = jedisPool.getResource();

		try {
			String value = jedis.get(prepend + target);

			if(!StringUtils.isEmpty(value)) {
				SharedData sharedData = CryptoUtil.encrypt(password, salt, value);
				String iv64 = Base64.encodeBase64String(sharedData.getIv());
				String cipher64 = Base64.encodeBase64String(sharedData.getCiphertext());
				String iv64Encoded = URLEncoder.encode(iv64, "UTF-8");
				String cipher64Encoded = URLEncoder.encode(cipher64, "UTF-8");
				String responseData = "iv64=" + iv64Encoded + "&cipher64=" + cipher64Encoded;

				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("text/plain;charset=utf-8");
				response.getWriter().print(responseData);

				return true;
			}
		} finally {
			jedisPool.returnResource(jedis);
		}

		return false;
	}

	private boolean handlePost(String target, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String iv64 = request.getParameter("iv64");
		String cipher64 = request.getParameter("cipher64");

		if(StringUtils.isEmpty(iv64) || StringUtils.isEmpty(cipher64)) {
			return false;
		}

		String value = CryptoUtil.decrypt(password, salt, new SharedData(Base64.decodeBase64(iv64), Base64.decodeBase64(cipher64)));
		Jedis jedis = jedisPool.getResource();

		try {
			String status = jedis.set(prepend + target, value);

			if("OK".equals(status)) {
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("text/plain;charset=utf-8");
				response.getWriter().println("OK");
			} else {
				throw new Exception("Redis status response: " + status);
			}
		} finally {
			jedisPool.returnResource(jedis);
		}

		return true;
	}

	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		baseRequest.setHandled(true);

		if(log.isDebugEnabled()) {
			log.debug(request.getMethod() + " from " + request.getRemoteAddr() + " for target: " + target);
		}

		try {
			if("GET".equals(request.getMethod())) {
				if(handleGet(target, response)) {
					return;
				}
			} else if("POST".equals(request.getMethod())) {
				if(handlePost(target, request, response)) {
					return;
				}
			}
		} catch(Exception e) {
			log.error("Error processing " + request.getMethod() + " for target: " + target, e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.setContentType("text/plain;charset=utf-8");
			response.getWriter().println("Secret error. Go away.");
		}

		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().println("<b>Not found: </b> " + target);
	}

	public static void main(String[] args) throws Exception {
		String realm = System.getProperty("MAJIG_REALM");
		String password = System.getProperty("MAJIG_PASSWORD");
		String salt64 = System.getProperty("MAJIG_SALT64");
		String port = System.getProperty("MAJIG_PORT");

		if(StringUtils.isEmpty(realm)) {
			throw new Exception("Missing realm");
		}

		if(StringUtils.isEmpty(password)) {
			throw new Exception("Missing password");
		}

		if(StringUtils.isEmpty(salt64)) {
			throw new Exception("Missing salt64");
		}

		if(StringUtils.isEmpty(port) || !StringUtils.isNumeric(port)) {
			throw new Exception("Missing or invalid port");
		}

		if(log.isDebugEnabled()) {
			log.debug("Realm = " + realm + ", Password = " + password + ", Salt64 = " + salt64);
		}

		JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), "localhost");
		Main main = new Main(password.toCharArray(), Base64.decodeBase64(salt64), realm, jedisPool);
		Server server = new Server();
		Connector connector = new SelectChannelConnector();

		connector.setHost("localhost");
		connector.setPort(Integer.parseInt(port));
		server.addConnector(connector);
		server.setHandler(main);
		server.start();
		server.join();
	}
}
