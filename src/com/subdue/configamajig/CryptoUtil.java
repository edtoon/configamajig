package com.subdue.configamajig;

import java.security.AlgorithmParameters;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptoUtil {
	private static final int KEYLEN_BITS = 256;
	private static final int ITERATIONS = 65536;

	public static final SharedData encrypt(char[] password, byte[] salt, String data) throws Exception {
		SecretKey secret = createSecretKey(password, salt);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		cipher.init(Cipher.ENCRYPT_MODE, secret);

		AlgorithmParameters params = cipher.getParameters();

		return new SharedData(
			params.getParameterSpec(IvParameterSpec.class).getIV(),
			cipher.doFinal(data.getBytes("UTF-8"))
		);
	}

	public static final String decrypt(char[] password, byte[] salt, SharedData sharedData) throws Exception {
		SecretKey secret = createSecretKey(password, salt);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(
				sharedData.getIv()));

		return new String(cipher.doFinal(sharedData.getCiphertext()), "UTF-8");
	}

	private static final SecretKey createSecretKey(char[] password, byte[] salt) throws Exception {
		SecretKeyFactory factory = SecretKeyFactory.getInstance(
				"PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEYLEN_BITS);
		SecretKey tmp = factory.generateSecret(spec);

		return new SecretKeySpec(tmp.getEncoded(), "AES");
	}

	public static class SharedData {
		private final byte[] iv;
		private final byte[] ciphertext;

		public SharedData(byte[] iv, byte[] ciphertext) {
			this.iv = iv;
			this.ciphertext = ciphertext;
		}

		public final byte[] getIv() {
			return iv;
		}

		public final byte[] getCiphertext() {
			return ciphertext;
		}
	}
}
