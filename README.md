configamajig
============

Shared-secret configuration repository. Slow, insecure, frankly just reprehensible.

Really just a wrapper for a local Redis instance that's a tiny bit better than leaving the Redis port open to everyone.


How to run
==========

ant -DMAJIG_REALM=myapp -DMAJIG_PASSWORD=shared_secret -DMAJIG_SALT64=base64_salt -DMAJIG_PORT=9876 run

You shouldn't really do that though. You shouldn't really use this at all..
